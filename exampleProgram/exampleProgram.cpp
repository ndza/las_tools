// Uses shared library to create and print a LAS file.

#include "las_tools/las_tools.h"
#include <iostream>


using namespace las_tools;


int main(void)
{
    las_file lf;

    lf.add_mnemonic_to_section(las_section_type_W, "STRT", "M", "123.45", "START DEPTH");
    lf.add_mnemonic_to_section(las_section_type_W, "STOP", "M", "124.45", "STOP DEPTH");
    lf.add_mnemonic_to_section(las_section_type_W, "STEP", "M", "0.20", "STEP");

    lf.add_mnemonic_to_section(las_section_type_C, "DEPT", "M", "", "DEPTH");
    lf.add_mnemonic_to_section(las_section_type_C, "DT", "US/M", "60 520 32 00", "SONIC TRANSIT TIME");

    lf.add_comment_to_section(las_section_type_A, "DEPT DT");

    lf.add_line_to_section(las_section_type_A, "123.45  123.45");
    lf.add_line_to_section(las_section_type_A, "123.65  123.45");
    lf.add_line_to_section(las_section_type_A, "123.85  123.45");

    lf.add_comment_to_section(las_section_type_A, "DEPT DT");

    lf.add_line_to_section(las_section_type_A, "124.05  123.45");
    lf.add_line_to_section(las_section_type_A, "124.25  123.45");
    lf.add_line_to_section(las_section_type_A, "124.45  123.45");

    lf.add_comment_to_section(las_section_type_A, "DEPT DT");

    std::cout << lf << std::endl;

    return 0;
}
