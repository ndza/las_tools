#include "las_tools/las_tools.h"
#include "las_tools/private/las_tools_impl.h"


namespace las_tools
{


las_file::las_file():
        model(new las_model())
{
}


las_file::~las_file()
{
}


void las_file::add_mnemonic_to_section(const las_section_type lst, const std::string& mnem, const std::string& units, const std::string& data, const std::string& desc)
{
    switch(lst)
    {
    case las_section_type_W:
    case las_section_type_C:
    case las_section_type_P:
    {
        auto section = model->get_section(lst);
        section->add_mnemonic(las_mnemonic{mnem, units, data, desc});
        break;
    }
    default:
        break;
    }
}


void las_file::add_line_to_section(const las_section_type lst, const std::string& line)
{
    switch(lst)
    {
    case las_section_type_O:
    case las_section_type_A:
    {
        auto section = model->get_section(lst);
        section->add_line(line);
        break;
    }
    default:
        break;
    }
}


void las_file::add_comment_to_section(const las_section_type lst, const std::string& line)
{
    auto section = model->get_section(lst);
    if (section) {
        section->add_comment(line);
    }
}


std::ostream& operator<<(std::ostream& os, const las_tools::las_file& lf)
{
    os << *(lf.model);
    return os;
}


}
