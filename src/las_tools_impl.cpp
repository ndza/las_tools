#include "las_tools/las_tools.h"
#include "las_tools/private/las_tools_impl.h"

#include <array>


namespace las_tools
{


const char* las_section_type_to_str(const las_section_type lst)
{
    switch(lst)
    {
    case las_section_type_V:
        return "VERSION INFORMATION SECTION";
        break;
    case las_section_type_W:
        return "WELL INFORMATION SECTION";
        break;
    case las_section_type_C:
        return "CURVE INFORMATION SECTION";
        break;
    case las_section_type_P:
        return "PARAMETER INFORMATION SECTION";
        break;
    case las_section_type_O:
        return "OTHER";
        break;
    case las_section_type_A:
        return "ASCII LOG DATA";
        break;
    }
    return "";
}


las_mnemonic::las_mnemonic(const std::string& mnem, const std::string& units, const std::string& data, const std::string& desc):
    mnem(mnem),
    units(units),
    data(data),
    desc(desc)
{
}


std::ostream& operator<<(std::ostream& os, const las_mnemonic& lmnem)
{
    os << lmnem.mnem << "." << lmnem.units << " " << lmnem.data << " :" << lmnem.desc << "\r\n";
    return os;
}


las_section::las_section():
    index(0)
{
}


void las_section::add_comment(const std::string& line)
{
    comments.push_back({index, line});
}


int las_section::stream_comments(std::ostream& os, int comment_i, int item_i) const
{
    while (comment_i < comments.size() && comments[comment_i].first <= item_i)
    {
        os << "#" << comments[comment_i].second << "\r\n";
        ++comment_i;
    }
    return comment_i;
}


std::ostream& operator<<(std::ostream& os, const las_section& ls)
{
    ls.stream_out(os);
    return os;
}


void las_section_with_mnemonics::add_mnemonic(const las_mnemonic& mnem)
{
    mnemonics.push_back(mnem);
    index++;
}


void las_section_with_mnemonics::stream_out(std::ostream& os) const
{
    int i = 0;
    int comment_i = 0;

    for(const auto& mnem: mnemonics)
    {
        comment_i = stream_comments(os, comment_i, i);
        os << mnem;
        ++i;
    }
    comment_i = stream_comments(os, comment_i, i);
}


void las_section_with_lines::add_line(const std::string& line)
{
    lines.push_back(line);
    index++;
}


void las_section_with_lines::stream_out(std::ostream& os) const
{
    int i = 0;
    int comment_i = 0;

    for(const auto& line: lines)
    {
        comment_i = stream_comments(os, comment_i, i);

        os << line << "\r\n";
        ++i;
    }
    comment_i = stream_comments(os, comment_i, i);
}


las_model::las_model()
{
    sections[las_section_type_W] = std::make_shared<las_section_with_mnemonics>();
    sections[las_section_type_C] = std::make_shared<las_section_with_mnemonics>();
    sections[las_section_type_P] = std::make_shared<las_section_with_mnemonics>();
    sections[las_section_type_O] = std::make_shared<las_section_with_lines>();
    sections[las_section_type_A] = std::make_shared<las_section_with_lines>();
}


std::shared_ptr<las_section> las_model::get_section(const las_section_type lst)
{
    return sections[lst];
}


std::ostream& operator<<(std::ostream& os, const las_model& lm)
{
    os << "~"<< las_section_type_to_str(las_section_type_V) << "\r\n";
    os << "VERS. 2.0 :CWLS LOG ASCII STANDARD - VERSION 2.0\r\n";
    os << "WRAP. NO :One line per depth step\r\n";

    std::array<las_section_type, 5> lsts = {las_section_type_W, las_section_type_C, las_section_type_P, las_section_type_O, las_section_type_A};

    for(const auto& lst: lsts)
    {
        os << "~"<< las_section_type_to_str(lst) << "\r\n";
        os << *(lm.sections.at(lst));
    }
    return os;
}

}
