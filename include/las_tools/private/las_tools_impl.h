#pragma once

#include "las_tools/las_tools.h"

#include <map>
#include <memory>
#include <vector>


namespace las_tools
{


const char* las_section_type_to_str(const las_section_type lst);


class las_mnemonic
{
public:
    las_mnemonic(const std::string& mnem, const std::string& units, const std::string& data, const std::string& desc);
    ~las_mnemonic() {}

    friend std::ostream& operator<<(std::ostream& os, const las_mnemonic& lm);

private:
    std::string mnem;
    std::string units;
    std::string data;
    std::string desc;
};


class las_section
{
public:
    las_section();
    virtual ~las_section() {}

    friend std::ostream& operator<<(std::ostream& os, const las_section& ls);

    virtual void add_mnemonic(const las_mnemonic& mnem) {}

    virtual void add_line(const std::string& line) {}

    virtual void add_comment(const std::string& line);

protected:
    virtual void stream_out(std::ostream& os) const = 0;

    int stream_comments(std::ostream& os, int comment_i, int item_i) const;

    int index;
    std::vector<std::pair<int, std::string>> comments;
};


class las_section_with_mnemonics: public las_section
{
public:
    las_section_with_mnemonics() {}

    virtual void add_mnemonic(const las_mnemonic& mnem);

protected:
    virtual void stream_out(std::ostream& os) const;

private:
    std::vector<las_mnemonic> mnemonics;
};


class las_section_with_lines: public las_section
{
public:
    las_section_with_lines() {}

    virtual void add_line(const std::string& line);

protected:
    virtual void stream_out(std::ostream& os) const;

private:
    std::vector<std::string> lines;
};


class las_model
{
public:
    las_model();
    virtual ~las_model() {}

    friend std::ostream& operator<<(std::ostream& os, const las_model& lm);

    std::shared_ptr<las_section> get_section(const las_section_type lst);

private:
    std::map<las_section_type, std::shared_ptr<las_section>> sections;
};


}
