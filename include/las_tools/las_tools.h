// Log ASCII Standard - main header file.
#pragma once

#include <memory>
#include <ostream>


namespace las_tools
{


typedef char las_section_type;

const las_section_type las_section_type_V = 'V';
const las_section_type las_section_type_W = 'W';
const las_section_type las_section_type_C = 'C';
const las_section_type las_section_type_P = 'P';
const las_section_type las_section_type_O = 'O';
const las_section_type las_section_type_A = 'A';


class las_model;


class las_file
{
public:
    las_file();
    virtual ~las_file();

    las_file(const las_file&) = delete;
    las_file(const las_file&&) = delete;
    las_file& operator=(const las_file&) = delete;
    las_file& operator=(const las_file&&) = delete;

    friend std::ostream& operator<<(std::ostream& os, const las_file& lf);

    /**
     * Add a mnemonic to the W, C, P section.
     */
    void add_mnemonic_to_section(const las_section_type lst, const std::string& mnem, const std::string& units, const std::string& data, const std::string& desc);

    /**
     * Add data to the O, A section.
     */
    void add_line_to_section(const las_section_type lst, const std::string& line);

    /**
     * Add a comment to any section.
     */
    void add_comment_to_section(const las_section_type lst, const std::string& line);

protected:
    std::unique_ptr<las_model> model;
};


}
